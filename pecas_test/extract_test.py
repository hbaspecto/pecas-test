"""
Utility for extracting sub-files from a file for use in test cases.
"""
from os import path


def main(in_path, out_path, **kwargs):
    ranges = {colname: set(r.list) for colname, r in kwargs.items()}

    _, extension = path.splitext(in_path)

    if extension == ".csv":
        extract_csv(in_path, out_path, ranges)
    elif extension == ".omx":
        extract_omx(in_path, out_path, ranges)
    else:
        raise ValueError("Unsupported extension " + extension)


def extract_csv(in_path, out_path, ranges):
    import csv
    with open(in_path) as in_file:
        with open(out_path, "w", newline="") as out_file:
            reader = csv.reader(in_file)
            writer = csv.writer(out_file)

            header = next(reader)
            writer.writerow(header)

            for line in reader:
                for colname, valid in ranges.items():
                    if int(line[header.index(colname)]) not in valid:
                        break
                else:
                    writer.writerow(line)


def extract_omx(in_path, out_path, ranges):
    import numpy as np
    import openmatrix as omx

    # noinspection PyTypeChecker
    in_file: omx.File = omx.open_file(in_path)
    # Check validity of mapping names
    [in_file.mapping(name) for name in ranges.keys()]

    mapping_names = in_file.list_mappings()
    matrix_names = in_file.list_matrices()

    mappings = {name: in_file.mapping(name) for name in mapping_names}
    matrices = {name: np.array(in_file[name]) for name in matrix_names}

    keeping_rows = set(range(in_file.shape()[0]))

    for name, valid in ranges.items():
        valid_rows = {
            row_number for zone_number, row_number in mappings[name].items()
            if zone_number in valid
        }
        keeping_rows &= valid_rows

    keeping_rows = sorted(keeping_rows)

    in_file.close()

    # noinspection PyTypeChecker
    out_file: omx.File = omx.open_file(out_path, "w")

    for name, matrix in matrices.items():
        out_matrix = np.zeros((len(keeping_rows), len(keeping_rows)))
        for i, i_row in enumerate(keeping_rows):
            for j, j_row in enumerate(keeping_rows):
                out_matrix[i, j] = matrix[i_row, j_row]

        out_file[name] = out_matrix

    for name, mapping in mappings.items():
        reversed_mapping = {index: label for label, index in mapping.items()}
        out_mapping = np.array([reversed_mapping[row] for row in keeping_rows])

        out_file.create_mapping(name, out_mapping)

    out_file.close()
