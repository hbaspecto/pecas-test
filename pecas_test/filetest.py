import os
import unittest
from os.path import join


class FileTestCase(unittest.TestCase):
    def assertFilesEqual(self, expected_path, actual_path, ignore_lines_starting_with=None):
        with open(expected_path, "r") as expected_file:
            with open(actual_path, "r") as actual_file:
                expected_lines = list(expected_file)
                actual_lines = list(actual_file)
                if ignore_lines_starting_with is not None:
                    expected_lines = [
                        line for line in expected_lines
                        if line != "\n" and not line.startswith(ignore_lines_starting_with)
                    ]
                    actual_lines = [
                        line for line in actual_lines
                        if line != "\n" and not line.startswith(ignore_lines_starting_with)
                    ]
                self.assertEqual(
                    expected_lines,
                    actual_lines,
                    f"\nFiles {expected_path} and {actual_path} differ"
                )

    def assertDirectoriesEqual(self, expected_dir, actual_dir, ignore_lines_starting_with=None):
        expected_files = sorted(os.listdir(expected_dir))
        actual_files = sorted(os.listdir(actual_dir))
        self.assertEqual(expected_files, actual_files)
        for expected_file, actual_file in zip(expected_files, actual_files):
            self.assertFilesEqual(
                join(expected_dir, expected_file),
                join(actual_dir, actual_file),
                ignore_lines_starting_with
            )
