import hbautil.scriptutil as su
import pecas_routines as pr
from pecas_test import mockscen


def create_database(ps):
    mockscen.create_database(ps, mapit=True)
    q = pr.mapit_querier(ps)
    q.query_external(su.in_this_directory(__file__, "mockmapit.sql"))

