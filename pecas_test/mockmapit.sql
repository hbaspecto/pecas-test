create schema output;

        
create table output.aa_output_files
(
    file_id integer,
    csv_file_name varchar,
    all_table_name varchar,
    temp_table_name varchar,
    activity_commodity_field_name varchar,
    zone_field_name varchar,
    is_active boolean,
    all_view_name varchar,
    zone_type varchar,
    partitioned boolean,
    quantity_field_name varchar,
    primary key (file_id)
);

        
insert into output.aa_output_files values
(1, 'ActivitySummary.csv', 'all_activity_summary', 'activity_summary_temp',
    'activity', null, true, 'all_activity_summary_3', null, false, null),
(4, 'ExchangeResultsTotals.csv', 'all_exchange_results_totals', 'exchange_results_totals_temp',
    'commodity', null, true, 'all_exchange_results_totals', null, false, null),
(17, 'FloorspaceSD.csv', 'all_floorspacesd', 'floorspacesd_temp',
    'aa_commodity', 'taz', true, 'all_floorspacesd_3', 'taz', false, null),
(11, 'ActivityLocations2.csv', 'all_activity_locations_2', 'activity_locations_2_temp',
    'activity', 'zonenumber', true, 'all_activity_locations_2_3', 'taz', false, null),
(60, 'Employment.csv', 'all_employment', 'employment_temp',
    null, 'taz', true, 'all_employment', 'taz', false, null),
-- Bogus table to test that the uploader doesn't try to upload files just because they're in this table
(42, 'Answers.csv', 'all_answers', 'answers_temp',
    null, 'taz', true, 'all_answers', 'taz', false, null);

        
create table output.all_activity_summary
(
    year_run integer,
    scenario varchar,
    activity varchar,
    compositeutility varchar,
    size double precision,
    primary key (year_run, scenario, activity)
);

create table output.activity_summary_temp
(
    activity varchar,
    compositeutility varchar,
    size double precision,
    primary key (activity)
);

create table output.all_exchange_results_totals
(
    year_run integer,
    scenario varchar,
    commodity varchar,
    demand double precision,
    internalbought double precision,
    exports double precision,
    supply double precision,
    internalsold double precision,
    imports double precision,
    rmssurplus double precision,
    averageprice double precision,
    primary key (year_run, scenario, commodity)
);

create table output.exchange_results_totals_temp
(
    commodity varchar,
    demand double precision,
    internalbought double precision,
    exports double precision,
    supply double precision,
    internalsold double precision,
    imports double precision,
    rmssurplus double precision,
    averageprice double precision,
    primary key (commodity)
);

create table output.all_floorspacesd
(
    year_run integer,
    scenario varchar,
    taz integer,
    aa_commodity varchar,
    quantity double precision,
    primary key (year_run, scenario, taz, aa_commodity)
);

create table output.floorspacesd_temp
(
    taz integer,
    aa_commodity varchar,
    quantity double precision,
    primary key (taz, aa_commodity)
);

create table output.all_activity_locations_2
(
    year_run integer,
    scenario varchar,
    activity varchar,
    zonenumber integer,
    quantity double precision,
    primary key (year_run, scenario, activity, zonenumber)
);

create table output.activity_locations_2_temp
(
    activity varchar,
    zonenumber integer,
    quantity double precision,
    primary key (activity, zonenumber)
);

create table output.all_employment
(
    year_run integer,
    scenario varchar,
    taz integer,
    naics integer,
    noc11 integer,
    amt numeric,
    primary key (year_run, scenario, taz, naics, noc11)
);

create table output.employment_temp
(
    taz integer,
    naics integer,
    noc11 integer,
    amt numeric,
    primary key (taz, naics, noc11)
);

create table output.loaded_scenarios
(
    scen_name varchar,
    start_year integer,
    end_year integer,
    primary key (scen_name)
);

CREATE OR REPLACE FUNCTION output.update_loaded_scenarios(
	scenario_name text,
	start_yr integer,
	end_yr integer)
    RETURNS void
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE
AS $BODY$
DECLARE
	arow output.loaded_scenarios%%ROWTYPE;
	v_start_year integer = start_yr;
	v_end_year   integer = end_yr;
BEGIN
	IF v_end_year IS NULL THEN
		v_end_year = v_start_year;
	END IF;

	IF v_end_year < v_start_year THEN
		-- SWAP 2 variables with no third temp variable :)
		v_end_year = v_end_year + v_start_year;
		v_start_year = v_end_year - v_start_year;
		v_end_year = v_end_year - v_start_year;
	END IF;

	RAISE NOTICE 'Updating Scenario %% with %% and %%', scenario_name, v_start_year, v_end_year;
	SELECT scen_name, start_year, end_year INTO STRICT arow FROM output.loaded_scenarios WHERE scen_name = scenario_name;
		IF arow.start_year > v_start_year THEN
			UPDATE output.loaded_scenarios SET start_year = v_start_year
			WHERE scen_name = scenario_name;
		ELSIF arow.end_year < v_end_year THEN
			UPDATE output.loaded_scenarios SET end_year = v_end_year
			WHERE scen_name = scenario_name;
		END IF;
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			INSERT INTO output.loaded_scenarios VALUES (scenario_name, v_start_year, v_end_year);
END;
$BODY$;

CREATE OR REPLACE FUNCTION output.clean_up_tables_for_scenario(
	scenario_name text)
    RETURNS void
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE
AS $BODY$
DECLARE
	rec RECORD ;
BEGIN
	FOR rec IN SELECT * FROM output.aa_output_files where file_id != 42 LOOP
		RAISE NOTICE 'Scenario %% being deleted from %%', scenario_Name , rec.all_table_name;
		begin
			EXECUTE 'SELECT output.' || rec.all_table_name || '__drop_partition($1);' USING scenario_Name ;
		exception when others then
			EXECUTE 'DELETE FROM output.' || rec.all_table_name || ' WHERE scenario = $1' USING scenario_Name ;
		end;
		EXECUTE 'TRUNCATE TABLE output.' || rec.temp_table_name;
	END LOOP;

	DELETE FROM output.loaded_scenarios
	WHERE scen_name = scenario_name;

	RAISE NOTICE 'Scenario %% is deleted from all tables', scenario_Name;
	--RETURN 0;
END;
$BODY$;
