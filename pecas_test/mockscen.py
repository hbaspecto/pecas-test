# Sets up a simple PECAS database to test the run scripts

import os
from os.path import join
import subprocess

import pecas_routines as pr


##########


def psql_sql(ps, sql, db_name=None):
    db_name = db_name or ps.sd_database
    os.environ["PGPASSWORD"] = ps.sd_password
    cmd = [
        join(ps.pgpath, 'psql'),
        '-h', ps.sd_host,
        '-p', str(ps.sd_port),
        '-U', ps.sd_user,
        '-c', sql,
        '-q',
        db_name
    ]
    subprocess.check_call(cmd)


def pg_restore(ps, path):
    os.environ["PGPASSWORD"] = ps.sd_password
    cmd = [
        join(ps.pgpath, "pg_restore"),
        '-h', ps.sd_host,
        '-p', str(ps.sd_port),
        '-U', ps.sd_user,
        '-d', ps.sd_database,
        '-c',
        path
    ]
    subprocess.check_call(cmd)


def pg_restore_plain(ps, path):
    os.environ["PGPASSWORD"] = ps.sd_password
    cmd = [
        join(ps.pgpath, 'psql'),
        '-h', ps.sd_host,
        '-p', str(ps.sd_port),
        '-U', ps.sd_user,
        '-f', path,
        '-q',
        ps.sd_database
    ]
    subprocess.check_call(cmd)


def create_database(ps, geometry=False, crosstab=False, mapit=False):
    db_name = ps.mapit_database if mapit else ps.sd_database
    psql_sql(ps, "drop database if exists {}".format(db_name),
             db_name="template1")
    psql_sql(ps, "create database {}".format(db_name),
             db_name="template1")
    querier = pr.mapit_querier(ps) if mapit else pr.sd_querier(ps)
    if geometry:
        querier.query("create extension postgis;")
    if crosstab:
        querier.query("create extension tablefunc;")


def set_up_tables(ps):
    with pr.sd_querier(ps).transaction() as tr:
        _create_schema(tr)
        _create_zones(tr)
        _create_space_types(tr)
        _create_parcels(tr)


def create_schema(ps):
    with pr.sd_querier(ps).transaction() as tr:
        _create_schema(tr)


def _create_schema(tr):
    tr.query("drop schema if exists {sch} cascade")
    tr.query("create schema if not exists {sch}")


def _create_zones(tr):
    tr.query(
        "create table {sch}.luzs\n"
        "(\n"
        "   luz_number integer primary key,\n"
        "   luz_name character varying\n"
        ")")

    tr.query(
        "create table {sch}.tazs\n"
        "(\n"
        "   taz_number integer primary key,\n"
        "   luz_number integer not null,\n"
        "   county_fips integer,\n"
        "   foreign key (luz_number) references "
        "{sch}.luzs (luz_number)\n"
        ")")


def _create_space_types(tr):
    tr.query(
        "create table {sch}.space_types_group\n"
        "(\n"
        "   space_types_group_id integer primary key,\n"
        "   space_types_group_name character varying(150),\n"
        "   cost_adjustment_damping_factor double precision\n"
        ")")

    tr.query(
        "create table {sch}.space_types_i\n"
        "(\n"
        "   space_type_id integer primary key,\n"
        "   space_type_name character varying(150),\n"
        "   space_type_code character varying(3),\n"
        "   age_rent_discount double precision,\n"
        "   density_rent_discount double precision,\n"
        "   age_maintenance_cost double precision,\n"
        "   add_transition_const double precision,\n"
        "   new_from_transition_const double precision,\n"
        "   new_to_transition_const double precision,\n"
        "   renovate_transition_const double precision,\n"
        "   renovate_derelict_const double precision,\n"
        "   demolish_transition_const double precision,\n"
        "   derelict_transition_const double precision,\n"
        "   no_change_transition_const double precision,\n"
        "   new_type_dispersion_parameter double precision,\n"
        "   gy_dispersion_parameter double precision,\n"
        "   gz_dispersion_parameter double precision,\n"
        "   gw_dispersion_parameter double precision,\n"
        "   gk_dispersion_parameter double precision,\n"
        "   nochange_dispersion_parameter double precision,\n"
        "   intensity_dispersion_parameter double precision,\n"
        "   maintenance_cost double precision,\n"
        "   space_type_group_id integer,\n"
        "   cost_adjustment_factor double precision,\n"
        "   construction_capacity_tuning_parameter double precision,\n"
        "   converting_factor_for_space_type_group double precision,\n"
        "   below_step_point_adjustment double precision,\n"
        "   above_step_point_adjustment double precision,\n"
        "   step_point double precision,\n"
        "   step_point_adjustment double precision,\n"
        "   min_intensity double precision,\n"
        "   max_intensity double precision,\n"
        "   foreign key (space_type_group_id) references "
        "{sch}.space_types_group (space_types_group_id)\n"
        ")")


def _create_parcels(tr):
    tr.query(
        "create table {sch}.parcels\n"
        "(\n"
        "   parcel_id character varying,\n"
        "   pecas_parcel_num bigint primary key,\n"
        "   year_built integer,\n"
        "   taz integer,\n"
        "   space_type_id integer,\n"
        "   space_quantity double precision,\n"
        "   land_area double precision,\n"
        "   available_services_code integer,\n"
        "   is_derelict boolean,\n"
        "   is_brownfield boolean,\n"
        "   foreign key (taz) references {sch}.tazs (taz_number),\n"
        "   foreign key (space_type_id) references "
        "{sch}.space_types_i (space_type_id)\n"
        ")")
