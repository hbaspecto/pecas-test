import os
import socket

import hbautil.scriptutil as su
from hbautil.settings import Settings, from_yaml
from hbautil import mapit_files


class PecasSettings(Settings):
    def __init__(self, scenario=None):
        args = dict(
            inputpath="AllYears/Inputs",
            postgres="postgres",
            sqlserver="sqlserver",
            use_sd=False,
        )
        if scenario is not None:
            args.update(scenario=scenario)
        super().__init__(**args)

    def with_scenario_directory(self, directory):
        return self.clone_with(scendir=directory)

    def with_machine_settings(self):
        local_machine_settings_fname = "machine_{}.yml".format(socket.gethostname().split(".")[0])
        local_machine_settings_path = su.in_this_directory(__file__, local_machine_settings_fname)
        local_machine_settings = from_yaml(local_machine_settings_path)
        return self.update(local_machine_settings).clone_with(sql_system="postgres")

    def between_years(self, start_year, end_year):
        return self.clone_with(
            baseyear=start_year,
            startyear=start_year,
            aa_startyear=start_year,
            sd_startyear=start_year,
            sd_backupyear=start_year,
            mapit_startyear=start_year,
            endyear=end_year,
            stopyear=end_year,
            aayears=su.irange(start_year, end_year),
            skimyears=[start_year],
        )

    def aa_only_in_odd_years(self):
        return self.clone_with(
            aayears=[year for year in self.aayears if year % 2 != 0]
        )

    def travel_model_in(self, *years):
        return self.clone_with(
            use_tm=True,
            tmyears=list(years),
            tm_startyear=min(years),
            earliest_squeeze_year=min(years)
        )

    def with_emp_pop(self):
        return self.clone_with(
            employment=True,
        )

    def with_sd(self, host=None, port=None, database=None, user=None, password=None, schema=None):
        return self.clone_with(
            use_sd=True,
            sd_host=(host or self.sd_host),
            sd_port=(port or self.sd_port),
            sd_database=(database or self.sd_database),
            sd_user=(user or self.sd_user),
            sd_password=(password or self.sd_password),
            sd_schema=(schema or self.sd_schema),
        )

    def with_test_sd(self, schema="t00"):
        return self.with_sd(
            host=os.environ.get("PGHOST", "localhost"),
            port=int(os.environ.get("PGPORT", 5432)),
            #database=os.environ.get("PGDATABASE", "hba_py_test"),
            database="hba_py_test",
            user=os.environ.get("PGUSER", "postgres"),
            password=os.environ.get("PGPASSWORD", "postgres"),
            schema=schema,
        )

    def with_mapit(self, scenario, host=None, port=None, database=None, user=None, password=None, schema=None):
        return self.clone_with(
            load_output_to_mapit=True,
            scenario=scenario,
            mapit_host=(host or self.sd_host),
            mapit_port=(port or self.sd_port),
            mapit_database=(database or self.sd_database),
            mapit_user=(user or self.sd_user),
            mapit_password=(password or self.sd_password),
            mapit_schema=(schema or self.sd_schema),
            **mapit_files.__dict__
        )
