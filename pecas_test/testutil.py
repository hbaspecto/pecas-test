import os
import shutil


def recreate_test_folder(path):
    try:
        shutil.rmtree(path)
    except FileNotFoundError:
        pass
    os.mkdir(path)
