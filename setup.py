from setuptools import setup

setup(
    name="pecas_test",
    version="1.0",
    packages=["pecas_test"],
    install_requires=[
        "openmatrix",
    ],
)
